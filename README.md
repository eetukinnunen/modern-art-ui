Final assignment for Lahti University of Applied Sciences / Coursera course Programming Mobile Applications for Android Handheld Systems.

In this application, you can use a horizontal scrollbar to change the colors of all the modern art squares except one. You can visit a website via a popup window.

Android Studio or another equivalent program is recommended if you want to run this software.

Finished in 11.11.2017