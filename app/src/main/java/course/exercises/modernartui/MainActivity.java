package course.exercises.modernartui;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SeekBar colorSeekBar = (SeekBar) findViewById(R.id.cSeekBar);
        colorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            int progressChanged = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                progressChanged = progress;
                colorChange(progressChanged);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });
    }

    // Gradually changes the colors based on seekBar value
    public void colorChange(int barValue)
    {
        LinearLayout layoutLeft = (LinearLayout) findViewById(R.id.layout1);
        LinearLayout[] childrenLeft = new LinearLayout[layoutLeft.getChildCount()];

        for (int i = 0; i < layoutLeft.getChildCount(); i++)
        {
            if (i != 1)
            {
                childrenLeft[i] = (LinearLayout) layoutLeft.getChildAt(i);
                childrenLeft[i].setBackgroundColor(Color.argb(0xFF, barValue, barValue, ((i+1)*100)));
            }
        }

        LinearLayout layoutRight = (LinearLayout) findViewById(R.id.layout2);
        LinearLayout[] childrenRight = new LinearLayout[layoutRight.getChildCount()];

        for (int i = 0; i < layoutRight.getChildCount(); i++)
        {
            childrenRight[i] = (LinearLayout) layoutRight.getChildAt(i);
            childrenRight[i].setBackgroundColor(Color.argb(0xFF, barValue, ((i+1)*75), ((i+1)*75)));

        }
    }

    // Creates the top burger menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater burgerInflater = getMenuInflater();
        burgerInflater.inflate(R.menu.burger_menu, menu);

        return true;
    }

    // When selecting an item from the menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.more_info:
                infoDialog();
                return true;
            default:
                return false;
        }
    }

    // Shows the information dialog
    public void infoDialog()
    {
        DialogFragment infoDialFrag = new InfoDialogFragment();
        infoDialFrag.show(getFragmentManager(), "infodialfrag");
    }
}
