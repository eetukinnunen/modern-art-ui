package course.exercises.modernartui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.Dialog;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class InfoDialogFragment extends DialogFragment {
    static private final String URL = "https://www.moma.org/";
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.info_box)
                .setPositiveButton(R.string.visit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Opens the web page
                        goToWebsite();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Dialog gets closed
                    }
                });
        return builder.create();
    }

    private void goToWebsite()
    {
        // Intent to use a browser
        Intent browserIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(URL));
        startActivity(browserIntent);
    }

}
